# Informationsquellen

## Allgemeines

- Oft ist Suchen einfacher.
- Auf das Alter der Doku achten. Ohne derartige Angabe wertlos.


## kurz, konkret

- `$ befehl -h` oder `$ befehl --help`
- `$ man befehl`

## man pages

- sections:
   1. Executable programs or shell commands
   2. System calls (functions provided by the kernel)
   3. Library calls (functions within program libraries)
   4. Special files (usually found in /dev)
   5. File formats and conventions, e.g. /etc/passwd
   6. Games
   7. Miscellaneous (including macro packages and conventions), e.g. man(7), groff(7)
   8. System administration commands (usually only for root)

- Suche nach Thema (keyword):  
    `$ man -k list`

- [ManKier](https://www.mankier.com/about) - *tries to make reading and man pages as convenient as possible.*
  - Gliederung am Rand, TL;DR
  - siehe: `less` und `tar`


## Webseiten
- [ubuntuusers Wiki](https://wiki.ubuntuusers.de/Wiki/Neue_Artikel/) - guter Einstieg, oft hilfreich
- [ubuntuusers Howtos](https://wiki.ubuntuusers.de/Wiki/Neue_Howtos/)
- [Wikipedia](https://en.wikipedia.org/wiki/Xubuntu)
- [Linux Bibel Oesterreich](https://www.linux-bibel-oesterreich.at/index.php?sid=39085bfb0e3fc2332bc64c0abadf94c3)
- [ ArchWiki](https://wiki.archlinux.org/) =  Arch Linux documentation
- [LinuxWiki.org](https://linuxwiki.de/)
- [Thomas-Krenn-Wiki](https://www.thomas-krenn.com/de/wiki/Hauptseite) - hardwarelastig
- home page des Projektes, z.B.
  - [Documentation of the GNU Project](https://www.gnu.org/doc/doc.html)
- [Read the Docs](https://readthedocs.org/)
  - free software documentation platform
  - “docs like code” workflows, keeping your code & documentation as close as possible
  - [Quellen auf GitHub](https://github.com/readthedocs/readthedocs.org)
  - 2021: 700 Million page views
- Suche nach: `Linux/Ubuntu Thema`


## User Support

- IRC, Mailing Lists, ...


## (Online-) Bücher

- [SelfLinux](https://www.selflinux.org/) -  ein umfassenden Hypertext-Tutorial
- [Debian GNU/Linux Anwenderhandbuch](https://debiananwenderhandbuch.de/) von Frank Ronneburg
- [Das Debian Administrationshandbuch](https://debian-handbook.info/browse/de-DE/stable/)


## Zeitschriften

- [LinuxUser](https://www.linux-community.de/magazine/linuxuser/) - für User
- [Linux-Magazin](https://www.linux-magazin.de/) für Admins, Profis


## News

- [Open-Source-Blog-Netzwerk](https://osbn.de/ticker/?p=1&view=list)
- [Heise: Linux und Open Source](https://www.heise.de/thema/Linux-und-Open-Source)
- [golem: Open Source](https://www.golem.de/specials/open-source/)


## Veranstaltungen

- [Chemnitzer Linux-Tage](https://de.wikipedia.org/wiki/Chemnitzer_Linux-Tage) - ein Wochenende im März, Aufzeichnungen
  - [ 11. und 12. März 2023](https://chemnitzer.linux-tage.de/2023/de)




## Aufgaben
- Suche nach Thema in Section 1, z.B. file, editor, 
- Warum ManKier konkret besser als man? Z.B. tar, rsync, gcc
- Auf welche Journals hat die BA Zugriff?
- Bei "User Support" aus Erfahrung Gutes eintragen.
- Ergänzungen
- anmelden, mitmachen, von BA als ??? anerkennen lassen (z.B. bei [ubuntuusers](https://wiki.ubuntuusers.de/Howto/Vorbereitung_auf_Videokonferenzen/))
