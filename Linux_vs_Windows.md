# Unterschiede zwischen Linux und Windows

Unix-Rechner (Cray-1: 1976)
![Cray1](./Pics.d/cray1.jpg)

Windows-PC (MS-DOS:1981, Windows 1.0: 1985)
![PC](./Pics.d/IBM-PC.jpg)

- Windows: ein Betriebssystem
- Linux-Distribution: Betriebssystem + tausende paketierte Anwendungen

## Unix-Philosophie

1. Schreibe Computerprogramme so, dass sie nur eine Aufgabe erledigen und diese gut machen.
2. Schreibe Programme so, dass sie zusammenarbeiten.
3. Schreibe Programme so, dass sie Textströme verarbeiten, denn das ist eine universelle Schnittstelle.  

- [mehr](https://de.wikipedia.org/wiki/Unix-Philosophie)


## News

- Ab Heute (10.1.2023) [Windows-8.1 ohne Support](https://www.heise.de/news/Nur-noch-eine-Woche-Zeit-Support-Ende-von-Windows-8-1-7448516.html). D.h. fast 3 Millionen unsichere Windows-Rechner in D.

## Details



- Windows = W:
- Linux = L:

### Philosophie

- W: Komplexer Aufbau, einfache Bedienung - $$
- L: Simple is beautiful

---

- registry vs. /etc
- eierlegende Wollmilchsau vs. viele einfache Tools
- Single-User-System vs. Multi-User-System
- PowerShell vs. üblichem Terminal
- Unmündiger Anwender erwartet vs. Mündiger Anwender vorausgesetzt

### Standards

- W: Setzen von Industriestandards um Profit zu maximieren
- L: Nutzung freier Standards, um Verbreitung zu erhöhen; [POSIX](https://de.wikipedia.org/wiki/Portable_Operating_System_Interface), X/Open, [RFC](https://de.wikipedia.org/wiki/Request_for_Comments), ...

### Entwicklung

- W: Marketing-getrieben
- L: Anwender-getrieben

### Entwicklungsmodell

- W: nur Microsoft bestimmt
- L: [200 - 300](https://www.heise.de/select/ct/2019/16/1564403599540881) Firmen tragen bei, Linus Torvalds hat das letzte Wort (Do-Ocracy)

### Programmcode

- W: nicht vollständig offengelegt und schlecht dokumentiert
- L: (fast, z.B. kundenspezifische Patches von RH) alles frei

### Versionen

- W: nur eine favorisierte von Microsoft
- L: nur wenige favorisierte Kernel, aber [zahllose Distributionen](https://de.wikipedia.org/wiki/Liste_von_Linux-Distributionen) = Chance und Problem

### Lizenz

- W: günstige, ungefährliche Lizensierung ohne [Beratung](http://www.kerstin-friedrich.com/) nur in Sonderfällen möglich
  - z.B. CALs
- L: Kernel:  GPLv2, Rest: meist FOSS


### Kosten

- W: sehr unterschiedlich
  - Druck zur Cloud, Chronifizierung der Einnahmen
- L: meist keine (TCO kann trotzdem hoch sein: Schulung, Support)

### Ressourcen-Bedarf

- W: höher
- L: minimal bis hoch


### Benutzerfreundlichkeit

- W: für Nutzer ohne/mit geringen IT-Kenntnisse, viele Automatismen, Halbwissen genügt (?)
- L: manchmal hohe Einstiegshürden
  - [The Luxury of Ignorance: An Open-Source Horror Story](http://www.catb.org/~esr/writings/cups-horror.html), [Kurzform](https://www.heise.de/newsticker/meldung/Eric-Raymond-schimpft-auf-CUPS-94365.html), [dt. Übersetzung](https://kubieziel.de/computer/cups-horror.html)
  - [Wenn man's weiss ist alles ganz einfach!](https://gnulinux.ch/probleme-und-gnu-linux-wenn-man-s-weiss-ist-alles-ganz-einfach) - und umgekehrt


### GUI

- W: normal, Standards gesetzt, Programme ähneln sich
- L: gut, schlecht, gar nicht

### Support

- W: durch viele Firmen, weil so verbreitet; praktisch nicht vom Hersteller
- L: freundliche Communities (Kultur: man hilft sich im Netz), durch wenige Firmen, teure Linux-Abos

### Software

- W: für fast alles (Fachanwendungen)
- L: weniger Anwendungen, manche Bereiche fehlen weitgehend (z.B. Fachanwendungen der Verwaltungen)

### Spiele

- W: optimal
- L: nur rel. wenige, Emulation oft problematisch, deutliche Verbesserung der Lage dank [Steam](https://de.wikipedia.org/wiki/Steam)

### Desktop-Umgebung

- W: nur eine,  GUI und Windows zu einer untrennbaren Einheit verschmolzen. Nachteile: keine Auswahl, Komplexität versteckt, Fehlersuche schwieriger, Automatisierung schwieriger, Power-User lieben CLI
- L: ist auch nur eine graphische Applikation, kann man austauschen, [>20](https://de.wikipedia.org/wiki/Desktop-Umgebung#Galerie)

### Installation

- W: muss laufen
- L: bei passender Hardware-Auswahl unkritisch, Gegenteil auch richtig

### Deinstallation

- W: oft problematisch
- L: unproblematisch dank Paket-System

### Hardware-Unterstützung

- W: Windows muss laufen; meist vorinstalliert
- L: teilweise fehlen Treiber, daher vorher informieren; selten vorinstalliert bzw. kein OS installiert

### CPU-Unterstützung

- W: Intel, AMD
- L: für etwa 30 Prozessorplattformen verfügbar


### Stabilität

- W: besser geworden
- L: besser

### Sicherheit

- W: schlechter;  schlechtere Trennung Admin - User
- L: besser, auch weil kleinerer Markt;  Trennung root - User von Anfang an (Multi-User-System)

### Geschwindigkeit

- W: nimmt im Lauf der Zeit ab
- L: bleibt hoch

### Updates

- W: inzwischen zwangsweise
- L: eigenverantwortlich, Security-Patches oft früher

### Drucken

- W: kein Problem
- L: nicht unkritisch, PostScript-Drucker als Druckmodell, vor Kauf informieren


## siehe auch

- [Vergleich Windows versus Linux](https://www.ostc.de/windows.html)
- [Welches Betriebssystem für welchen Zweck?](https://www.wintotal.de/windows-vs-linux/)
- [Linux vs. Windows Betriebssysteme im Vergleich](https://www.ionos.de/digitalguide/server/knowhow/linux-vs-windows-betriebssystem/)


## Aufgaben

- Verbreitung/Marktanteil von Linux auf Servern, auf Desktop in den Jahren 2000, 2010, 2020
- Verbreitung/Marktanteil von Android in den Jahren 2000, 2010, 2020
