# Die Vorlesung

- [t1p.de/Linux](https://t1p.de/Linux)
- Mitmachen: https://t1p.de/linuxpad


## Ziele

- IT-Anforderungen im Beruf mit Linux bearbeiten
  - dafür kompetente Auswahl der Hilfsmittel, Software, Services
- ein Einstieg, von dem man ggf. weitergehen kann
- auf dem Desktop übt man, qualifiziert man sich für den Server
- Erweiterung der beruflichen Perspektiven
- Alternativen zu kommerziellen Angeboten


## Inhalte

- siehe [ToC](./0-ToC.md)
- bewährte Tools
- Informationsmanagement
- Arbeitsumgebung, Desktop
  - Auswahl eines Desktops
  - Einrichtung der Arbeitsumgebung
  - Shell
- Vorstellung von Programmen zum Aufbau einer Linux-Server-Landschaft (Filer, Backup, Archiv, ...)
- Services im Netz
  - Server als Einstieg sehr/zu ambitioniert => Einstieg, nicht Administration
  - Linux-Eignung untersuchen
- auch Umfeld: Hardware, Arbeitsumgebung


## Stil der Vorlesung

- Konkretes: Beispiele statt [Metasprache](https://de.wikipedia.org/wiki/Backus-Naur-Form) zur Syntax-Erklärung
- viele Beispiele: Abstrahieren ist einfacher als vom Allgemeinen zum Konkreten fehlerfrei zu gelangen
- Ausgangspunkt ist die Aufgabe, nicht das Programm  ("Wir nehmen immer was vom Marktführer.")
- Vermittlung von Erfahrungen, googeln kann jeder
- keine Feature-Listen, sondern HowTos, erste Eindrücke
- mitmachen, ergänzen (in Übungen)
- Skript zur Vorlesung: Markdown, Hypertext
  - viele URLs
    - statt cut 'n paste
    - die Auswahl ist Empfehlung, googeln kann jeder
- Die Halbwertszeit des IT-Wissens liegt bei 2a, daher lebendes Dokument mit Gedächtnis.
  - Erfahrung hilft Entwicklungen zu beurteilen, aber nicht konkret.
- inhärente Dynamik und Fehlertoleranz dank Versionskontrolle ([git](https://gitlab.hrz.tu-chemnitz.de/users/sign_in))
- Anregungen, in welche Richtungen man sein Wissen vertiefen könnte



## siehe auch

![QR-Code](./Pics.d/t1p.de-Linux.QR.png)
