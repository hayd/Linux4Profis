# Woran erkennt man gute FOSS ?

Wie finde ich ein gutes freies Tool für .... ?

## Finden

- `apt-cache search xyz | grep xyz`
- suchen: _Linux best xyz_   => Vergleiche, Reviews, ...



## Versionsnummern

- [Semantic Versioning](https://semver.org/lang/de/)
  - MAJOR.MINOR.PATCH
  - MAJOR: nicht abwärtskompatibel
  - MINOR: neue Features
  - PATCH: weniger/andere Bugs
- 0.x: (jahrelang) auf dem Weg zum ersten stable release
- Linux-Kernel: Wenn Linus Torvalds die [Finger](https://linuxnews.de/2022/10/linux-6-0-mit-verbessertem-scheduler-freigegeben/) ausgehen ...
- gerade, ungerade Nummern

## Kriterien

### Entwicklung

- Von wann letztes stable release ([Emacs](https://de.wikipedia.org/wiki/Emacs) vs. [XEmacs](https://de.wikipedia.org/wiki/XEmacs))?
- last commit?
- Wo gehostet (github, gitlab, berlios.de)?
- release notes - Entwicklung: Tempo, Inhalte, bug fixes
- #Autoren
- charismatischer Anführer ([BDFL](https://en.wikipedia.org/wiki/Benevolent_dictator_for_life), Benevolent Dictator for Life)
  - Linux, Python, Perl, GNOME
  - [Do-ocracy](https://de.wikipedia.org/wiki/Do-ocracy): Wer handelt, der entscheidet.


### Finanzierung

- Geschäftsmodell: Womit verdienen Entwickler Geld?
  - Restaurant-Prinzip
  - Support ([MINIO](https://min.io/pricing)), Schulung
  - Enterprise-Features (z.B. LDAP-Anbindung) gegen Geld
- Wer finanziert Entwickler (z.B. Kernel: RH, IBM, Microsoft, ...)?
- Was will mit die Firma sonst noch verkaufen?
- Spenden (Blender, krita): Was passiert, wenn Spendenbüchse leer?


### Misc

- Integration in [Distribution](https://packages.ubuntu.com/): `$ apt-cache search xyz`
  - d.h. min. ein Maintainer findet die Software so gut und wichtig, dass er Arbeit in die Paketierung investiert.
- Unterstützung [offener Schnittstellen](https://www.se-trends.de/offene-schnittstellen-erzwingen/) 
- Hits bei Suche


### Funktionalität

Wenn bisher kein KO-Kriterium aufgetreten ist, ...

- Anforderungen zusammenstellen
- Erfüllung der Anforderungen in der Praxis testen
  - Anlernzeit, ähnlich vertrauten Programmen
  - Doku
  - Usability (z.B. zahl der nötigen Klicks)
  - usw.



## Übungen

- bester Filemanager
- bestes Snapshot-Tool
- bester Browser
- bester Text-Editor
- bester Teminal-Emulator
- bester Audioplayer
- bester Videoplayer
- PDF-Reader / PDF-Viewer
- bester Bildbetrachter
- kleinstes Office-Paket
