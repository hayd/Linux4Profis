
# Linux für den professionellen Einsatz

## Einstieg

- [Fragerunde](./Fragerunde.md)
- [Organisatorisches](./Organisatorisches.md)
- [Die Vorlesung](./Die_Vorlesung.md) - Zielgruppe, Stil
- [Informationsquellen](Info-Quellen.md)


## FOSS

- [Linux vs. Windows](./Linux_vs_Windows.md) - Unterschiede, Einsatzgebiete
- [Freie Software](./FOSS.md) - Was ist Freie Software?
- [Kriterien für gute Freie Software](./Kriterien4guteFOSS.md)


## Distributionen

- [Linux-Distributionen](./Distributionen.d/Distributionen.md)
- [Auswahl einer Distribution](./Distributionen.d/Distri-Wahl.md)
- [Debian-Pakete](./Distributionen.d/deb-Pakete.md) - xyz.deb
- [APT](./Distributionen.d/APT.md) - Advanced Packaging Tool
- [Snap-, Flatpack-Pakete](./Distributionen.d/Snap,Flatpak.md)
- [Pflege der Pakete](Distributionen.d/Pflege.md)


## Arbeitsumgebung

- [Was habe ich und wie viel?](./Orientierung.md)
- [guter Arbeitsplatz](./guter_Arbeitsplatz.md) - Empfehlungen
- [Liste der Desktop-Umgebungen](./Desktop-Liste.md)
- [Desktop-Anforderungen](./Desktop-Anforderungen.md)
- [XFCE](./XFCE.md) - ein Desktop Environment
- [XFCE konfigurieren](./XFCE-config.md)


## Linux

### Basics

- [Basics](./Basics.md)
- [Kernel](./Kernel.md)
- [Prozesse](./Prozesse.md)
- [Memory](./Memory.md)
- [cron](./cron.md) - regelmäßige Aufgabe

#### Filesysteme

- [VFS](./Filesystem.d/VFS.md) - das Virtual File System
- [Everything is a file](./Filesystem.d/Everything_is_a_file.md)
- [FHS](./Filesystem.d/FHS.md) - Filesystem Hierarchy Standard
- [Platten](./Filesystem.d/Disks.md) - etwas Hardware-KnowHow
- [SMART](./Filesystem.d/SMART.md) - Platten überwachen
- [Filesysteme](./Filesystem.d/Filesystem.md) - lange Listen und eine Auswahl
- [Rechte im Filesystem](./Filesystem.d/FS-Rechte.md)
- [Filesystem-Befehle](./Filesystem.d/Filesystem-Befehle.md)
- [Disk-Tools](./Filesystem.d/DiskTools.md) - Standard-Befehle und Tools


### Datenhaltung

- [Filer](./Filesystem.d/Filer.md)
- [Storage-Clouds](./Filesystem.d/Storage-Clouds.md)
- [Backup](./Filesystem.d/Backup.md)
- [Archivierung](./Filesystem.d/Archivierung.md)


## Die interaktive Arbeitsumgebung

- [CLI vs GUI](./CLI_vs_GUI.md)
- [Terminals](./Terminals.md)
- [Prompt](./Prompt.md)
- [Die Shell](./shell.md)


## Bash

- [bash](./Bash.d/bash.md)
- [Variable](./Bash.d/Variable.md)
- [interaktive Bash](./Bash.d/interaktive_Bash.md)
- [redirect](./Bash.d/bash-redirect.md)
- [globbing](./Bash.d/bash-globbing.md)
- [Reguläre Ausdrücke](./REs.md)
- [RE-Uebungen](./RE-Uebungen.md)  
- [grep](./Prgs.d/grep.md) und [sed](./Prgs.d/sed.md) REs im Einsatz
- [Shell-Scripte](./Bash.d/Shell-Scripte.md)
- Schleifen
  - [for](./Bash.d/for.md)
  - [while](./Bash.d/while.md)
  - [Schleife vorzeitig verlassen](./Bash.d/exit_loop.md)
- [if](./Bash.d/bash-if.md)
- [functions](./Bash.d/bash-functions.md)
- [echo](./Bash.d/echo.md) -  Ausgabe auf Bildschirm
- [read](./Bash.d/read.md) - Einlesen
- [1. Script](./Bash.d/mein_1.Script.md) - Gerüst für erste Versuche
- [dialog](./Bash.d/dialog.md)
- [Aufgaben](./Bash.d/Aufgaben.md)


## Informationsmanagement

- [aktuelle Notizen](./InformationsManagement.d/bisher.md) - eine Umfrage
- [Informationsmanagement](./InformationsManagement.d/InformationsManagement.md) - eine Einführung
- [Markdown](./InformationsManagement.d/Markdown.md) - ein Allzweck-Format
- [Markdown-Tools](./InformationsManagement.d/Markdown-Tools.md)
- [Versionsverwaltung](./InformationsManagement.d/Versionsverwaltung.md)
- [RCS](./Prgs.d/RCS.md) - die persönliche Versionsverwaltung
- [Wikis](./InformationsManagement.d/Wikis.md)
- [Zim](./Prgs.d/Zim.md) - personal Wiki
- [Recoll](./Prgs.d/Recoll.md) - Desktop-Suchmaschine
- [PDF](./PDF.d/PDF.md) - PDFs ansehen, editieren, Seiten ausschneiden, ...
- [Text-Vergleich](./InformationsManagement.d/Text-Vergleich.md)
- [pandoc](./Prgs.d/pandoc.md) - Format-Konvertierer
- [hunspell](./Prgs.d/hunspell.md)


## Texte

- [Browser](./Browser.md)
- [Office Pakete](Office-Pakete.md)
- [LaTeX](./LaTeX.md)
- [Scannen](./Scannen.md) - Digitalisierung von Papier, OCR
- [Drucken](./Drucken.md)


## Komprimieren

- [Komprimierung](./Prgs.d/Komprimierung.md)
- [gzip, bzip](./Prgs.d/gzip,bzip.md)
- [7z](./Prgs.d/7z.md)


## Daten transportieren

- [tar](./Prgs.d/tar.md) - Daten archivieren
- [rsync](./Prgs.d/rsync.md) - Daten spiegeln


## Multimedia

### Bilder

- [Viewer](./Prgs.d/Image-Viewer.md)
- [GIMP](./Prgs.d/gimp.md) - freie Photoshop-Alternative
- [Inkscape](./Prgs.d/Inkscape.md) - SVG-Editor
- [ImageMagick](./Prgs.d/ImageMagick.md)
- [Screenshot-Programme](./Prgs.d/Screenshot.md)
- [ExifTool](./Prgs.d/ExifTool.md)
- DigiKam - Bildverwaltungsprogramm


### Audio

- [pavucontrol](./Sound.md#pavucontrol) - Soundkontrolle
- [Audacity](./Sound.md#Audacity) -  grafischer Audio-Editor/-Recorder


### Video

- [VLC](./Prgs.d/VLC.md) - ein vollständiger Medienplayer
- [FFmpeg](./Prgs.d/FFmpeg.md) - Konverter für Video- und Audiomaterial
- [Video-Editoren](./Prgs.d/Video-Editoren.md)


## Videokonferenzen

- [Videokonferenz-Technik](./Videokonferenz-Technik.md)
- [Videokonferenz-Lösungen](./Videokonferenz-Loesungen.md)


## Security

- [Bedrohung](./Bedrohungen.md)
- [Rechte im File-System](./Filesystem.d/FS-Rechte.md)
- [Phishing](./Phishing.md)
- [Passworte](./Passworte.md)
- [SSH-Login](./SSH-Login.md)
- [Datei-Verschlüsselung](./Datei-Verschluesselung.md) mit OpenSSL
- [Ordner-Verschlüsselung](./Ordner-Verschluesselung.md) mit GoCryptFS


## Programme, Tools, Bordmittel

- [Firefox + Addons](./Prgs.d/Firefox.md)
- [Kalender](./Kalender.md)
- [Zeitpunkte](./Prgs.d/Zeiten_rechnen.md) addieren, subtrahieren
- [Komprimierung](./Prgs.d/Komprimierung.md)
- [Rechnen](./Prgs.d/Rechnen.md)
- [Screenshot](./Prgs.d/Screenshot.md)
- [Thunderbird + Addons](./Prgs.d/Thunderbird)
- [awk, cut](./Prgs.d/awk,cut.md)
- [csplit](./Prgs.d/csplit.md)
- [file](./Prgs.d/file.md)
- [find](./Prgs.d/find.md)
- [grep, agrep](./Prgs.d/grep.md)
- [more,less,cat](./Prgs.d/more,less,cat.md)
- [rsync](./Prgs.d/rsync)
- [sed](./Prgs.d/sed.md)
- [sort,uniq](./Prgs.d/sort,uniq.md)
- [tar](./Prgs.d/tar)
- [tr](./Prgs.d/tr.md)
- [wget](./Prgs.d/wget.md) - Web-Server (-Teile) kopieren
- [xeyes](./Prgs.d/xeyes.md)



## Ablauf

- [1.Tag](./Ablauf/1.Tag.md)
- [2.Tag](./Ablauf/2.Tag.md)
- [3.Tag](./Ablauf/3.Tag.md)
- [4.Tag](./Ablauf/4.Tag.md)
- [5.Tag](./Ablauf/5.Tag.md)
- [6.Tag](./Ablauf/6.Tag.md)
- [7.Tag](./Ablauf/7.Tag.md)  
- [8.Tag](./Ablauf/8.Tag.md)   <---
