# Organisatorisches


## Tage der Vorlesung

- [Stundenplan](./Stundenplan-Q1.2023.pdf)
- letzten Termin vorziehen ?
- Prüfung Ende Feb. ?


## VM

- Jeder hat eine VM mit Xubuntu und root-Rechten. Kann man kaputtspielen.
- Feste Plätze bedeuten immer gleiche VM.


## Übungen

- geforderte Ergebnisse mailen an:  e0014625@ba-sachsen.de

## Materialien

- [PDFs](https://owncloud.gwdg.de/index.php/s/TJ8jikG7S1n3DdZ)
- [Files](https://owncloud.gwdg.de/index.php/s/pDoDfGRSESjIz3N)  


## Prüfung

- mündlich, einzeln
- ca. 20 Min.
- Einstieg: Erläuterung eines vorbereiteten Scriptes
- oder lieber schriftlich ?
