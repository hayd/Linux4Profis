# Freie Software

- FOSS: Free and Open Source Software
- FLOSS: Free/Libre Open Source Software - L für andere Sprachen und wegen frei != gratis
- oft synonym verwendet: Freie Software, Open-Source-Software

## frei

- Freiheit, nicht  Freibier
- Die 4 Freiheiten (ussi)
  1. use
  2. study
  3. share
  4. improve


- [GPL](https://de.wikipedia.org/wiki/GNU_General_Public_License)
- [Richard Stallman](https://de.wikipedia.org/wiki/Richard_Stallman)
- [Richard Stallman on Libre Software](https://www.youtube.com/watch?v=jkCkEfNzfzg)


## Lizenzen

### GPL

- kompletter Haftungsausschluss, nach dt. Recht unzulässig
- arglistige Täuschung

### Copyleft-Prinzip

- Virulenz: Alle abgeleiteten Programme eines unter der GPL stehenden Werkes dürfen von Lizenznehmern nur dann verbreitet werden, wenn sie von diesen ebenfalls zu den Bedingungen der GPL lizenziert werden.

### BSD-Lizenz

- Berkeley Software Distribution
- ohne Copyleft
- für kommerzielle Nutzung
- Beispiele: [Chromium (web browser)](https://en.wikipedia.org/wiki/Chromium_(web_browser)), [Nginx ( web server)](https://en.wikipedia.org/wiki/Nginx)

### Praxis

- Software nicht unter GPL => bei Änderung alle Autoren fragen (Chef des RZG)

## Konsequenzen

### Prinzipien

- KISS: Keep it Simple, Stupid  oder Keep it Small and Simple
- ein Programm für eine Aufgabe, keine eierlegende Wollmilchsau
  - 2 Anreize seine Rechte (ussi) zu nutzen

### Merkmale

- auf Kooperation angelegt => freie Schnittstellen


## Digitale Souveränität

- = selbstbestimmten Nutzung und Gestaltung von Informationstechnik
- 3 Hauptziele: Möglichkeit zu wechseln, Möglichkeit zu gestalten, Einfluss auf Anbieter
- Beispiele, die nachdenklich machen sollten:
  - [Microsoft schaltet Linux-Bootloader ab](https://www.heise.de/select/ct/2022/20/2223813341738463324)
  - [Atlassian zwingt Kunden in die Cloud](https://www.heise.de/select/ix/2021/2/2029610133871592636)
  - [Abhängigkeit von Microsoft gefährdet die digitale Souveränität](https://www.heise.de/newsticker/meldung/EU-Experten-warnen-Abhaengigkeit-von-Microsoft-gefaehrdet-die-digitale-Souveraenitaet-3679559.html)
  - [Adobe schaltet seine Creative Cloud in Venezuela ab](https://www.heise.de/select/ct/2019/23/1573228655794252)
  - [Pro und Kontra Adobe Creative Cloud](https://www.markuswaeger.com/2013/06/03/pro-und-kontra-adobe-creative-cloud/)
  - [Adobe: Pantone-Farbpaletten benötigen künftig ein Abo](https://www.heise.de/news/Neues-Pantone-Lizenzmodell-Adobe-wirft-mehrere-Farbpaletten-aus-Photoshop-Co-7324477.html)
  - Ergebnisse der [Sondierung](https://www.tagesspiegel.de/politik/schriftgrosse-11-calibri--so-soll-die-ampel-koalition-gebaut-werden-4285528.html) der Ampel-Parteien müssen 2021 in Schriftgröße 11, [Calibri](https://de.wikipedia.org/wiki/Calibri), Zeilenabstand 1,5 ausgeschrieben werden.

## Siehe auch

- [Open-Source-Lizenzen – Grundlagen und Entscheidungshilfen](https://www.bitfactory.io/de/blog/open-source-lizenzen/)
- [ifrOSS](https://ifross.org) Institut für Rechtsfragen der Freien und Open Source Software


## Aufgabe

- guten Header für eigene Software unter GPL suchen
